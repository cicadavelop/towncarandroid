package com.towncar.app;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalUtil {
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("prf", Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences pref = context.getSharedPreferences("prf", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return editor;
    }

    //쉐어드에 데이터 저장 함수
    public static void setMap(Context context, String key, String value){
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(key,value);
        editor.commit();
    }

    public static String getMap(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String str = sharedPreferences.getString(key,null);
        return str;
    }
}
