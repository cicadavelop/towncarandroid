package com.towncar.app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends PActivity {

    public WebView webView;
    public RelativeLayout r;


    public ImageView splash_img;
    public TextView splash_text;
    public TextView splash_text2;
    public boolean isTime = false;

    Timer timer;
    String globalUrl = "http://cicadateam2.cafe24.com/html/Main/main.html";
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timer = new Timer(true);



        //스플래쉬 제거 타이머
        final Timer timerLoding = new Timer(true);
        final Handler handlerLoding = new Handler(){
            public void handleMessage(Message msg){
                isTime = true;
                timerLoding.cancel();

            }
        };
        TimerTask timerTaskLoding = new TimerTask() {
            @Override
            public void run() {
                Message msg = handlerLoding.obtainMessage();
                handlerLoding.sendMessage(msg);
            }
        };
        timerLoding.schedule(timerTaskLoding, 5000, 5000);

        r = findViewById(R.id.splash);

        splash_img = findViewById(R.id.splash_logo);
        Animation imageAnim = AnimationUtils.loadAnimation(this, R.anim.splash_img);
        splash_img.startAnimation(imageAnim);


        splash_text = findViewById(R.id.splash_text);
        Animation textAnim = AnimationUtils.loadAnimation(this, R.anim.splash_text);
        splash_text.startAnimation(textAnim);

        splash_text2 = findViewById(R.id.splash_text2);
        Animation textAnim2 = AnimationUtils.loadAnimation(this, R.anim.splash_text);
        splash_text2.startAnimation(textAnim2);





        webView = findViewById(R.id.web_webview);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClientClass());
        WebSettings webSettings = webView.getSettings();
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setTextZoom(100);
        webSettings.setSaveFormData(true);      // 폼 입력 값 저장 여부
        webSettings.setSupportZoom(false);       // 줌 사용 여부 : HTML Meta태그에 적어놓은 설정이 우선 됨
        webSettings.setBuiltInZoomControls(true); // 줌 사용 여부와 같이 사용해야 하는 설정(안드로이드 내장 기능)
        webSettings.setDisplayZoomControls(false); // 줌 사용 시 하단에 뜨는 +, - 아이콘 보여주기 여부
        webSettings.setJavaScriptEnabled(true); // 자바스크립트 사용 여부
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//        webSettings.setSupportMultipleWindows(true);
//        webSettings.setJava
        String userAgent = webView.getSettings().getUserAgentString();
        webView.getSettings().setUserAgentString(userAgent + "android");
        webView.addJavascriptInterface(new JavascriptInterface(), "label");

        //webView.setBackgroundColor(0x00000000);
        //webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);


//        webSettings.setUseWideViewPort(false);
//        webSettings.setDomStorageEnabled(true); // 웹뷰내의 localStorage 사용 여부
//        webSettings.setGeolocationEnabled(true); // 웹뷰내의 위치 정보 사용 여부
//        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); // 웹뷰내의 JS의 window.open()을 허용할 것인지에 대한 여부
        if (Build.VERSION.SDK_INT >= 16) {
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW); // HTTPS HTTP의 연동, 서로 호출 가능하도록
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();

            cookieManager.setAcceptCookie(true);

            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }
        webView.loadUrl(globalUrl);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    //브릿지 함수모음
    final class JavascriptInterface extends WebViewClient {
        @android.webkit.JavascriptInterface
        public void BridgeGetMap(String key) {
            String str = LocalUtil.getMap(getContext(), key);
            loadBridge("javascript:BridgeGetMapRst('"+str+"')");
        }
        @android.webkit.JavascriptInterface
        public void BridgeSetMap(String key, String value) {
            LocalUtil.setMap(getContext(), key, value);
        }

        @android.webkit.JavascriptInterface
        public void BridgeGetToken() {
            String str = LocalUtil.getMap(getContext(), "accesToken");
            loadBridge("javascript:BridgeGetMapRst('"+str+"')");
        }
        @android.webkit.JavascriptInterface
        public void BridgeSetToken(String value) {
            LocalUtil.setMap(getContext(), "accessToken", value);
        }
    }
    //브릿지 함수 리턴함수
    private void loadBridge(final String urls) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (webView != null) {
                    webView.loadUrl(urls);
                }
            }
        });
    }


    private class WebViewClientClass extends WebViewClient {



        //스플래쉬 제거 타이머
        final Handler handler = new Handler(){
            public void handleMessage(Message msg){
                if(isTime){
                Log.e("1111","1111");
                    webView.bringToFront();

                    Animation imageAnim = AnimationUtils.loadAnimation(getContext(), R.anim.layout_in_out);
                    webView.startAnimation(imageAnim);


                    Animation imageAnim2 = AnimationUtils.loadAnimation(getContext(), R.anim.layout_splash_out);
                    r.startAnimation(imageAnim2);

                    timer.cancel();
                }
            }
        };

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }
            @Override
            public boolean cancel() {
                return super.cancel();
            }
        };

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            webView.setVisibility(View.VISIBLE);
            if(!isTime){
                timer.schedule(timerTask, 5000, 5000);
            }

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            Log.w("URL 확인", url);


            if (url.startsWith("tel:")) {
                Intent call_phone = new Intent(Intent.ACTION_DIAL);
                call_phone.setData(Uri.parse(url));
                startActivity(call_phone);
                return true;
            }

            if (url != null && url.startsWith("intent://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);  // 새창을 여는 액티비티나, 팝업일때 이용하면 용이합니다.
                    // forbid launching activities without BROWSABLE category
                    intent.addCategory("android.intent.category.BROWSABLE");
                    // forbid explicit call
                    intent.setComponent(null);
                    // forbid Intent with selector Intent
                    intent.setSelector(null);
                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        startActivity(intent);
                    } else {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                        marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
                        startActivity(marketIntent);
                    }

                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (url != null && url.startsWith("market://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    // forbid launching activities without BROWSABLE category
                    intent.addCategory("android.intent.category.BROWSABLE");
                    // forbid explicit call
                    intent.setComponent(null);
                    // forbid Intent with selector Intent
                    intent.setSelector(null);
                    if (intent != null) {
                        startActivity(intent);
                    }
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
            view.loadUrl(url);
            return false; // 외부  URL 연결 가능 true일 경우 onpagestarted 함수 실행 (내부에서 url 실행)

        }
    }




    }



//    private void deleteStatusBar(){
//        decorView = getWindow().getDecorView();
//        uiOption = decorView.getSystemUiVisibility();
//        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH )
//            uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
//            uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
//        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
//            uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
//        decorView.setSystemUiVisibility( uiOption );
//    }
